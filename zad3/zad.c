#include<stdio.h>
#include<time.h>
#include<string.h>
#include<limits.h>
#include<gsl/gsl_blas.h>


double *a;
double *b;
double *c;

void init_arrays(){
	srand(time(NULL));

	//50 x 50
	a = malloc(250000*sizeof(double));
	b = malloc(250000*sizeof(double));
	c = malloc(250000*sizeof(double));

	for(int i=0; i<250000; i++){
		a[i] = 1.0*rand()/INT_MAX;
		b[i] = 1.0*rand()/INT_MAX;
	}
}

void measure_time(int start){
	static struct timespec tms;
	static struct timespec tme;
	if(start == 1){
		clock_gettime(CLOCK_REALTIME, &tms);
	}
	else if (start == 0){
		clock_gettime(CLOCK_REALTIME, &tme);
		double diff = tme.tv_sec + 1e-9 * tme.tv_nsec - (tms.tv_sec + 1e-9 * tms.tv_nsec);
		printf("%lf\n", diff);
	}
}

void calculate(char *type, int vect){
	if(strcmp(type, "blas") == 0){
		gsl_matrix_view A = gsl_matrix_view_array(a, vect, vect);
		gsl_matrix_view B = gsl_matrix_view_array(b, vect, vect);
		gsl_matrix_view C = gsl_matrix_view_array(c, vect, vect);
		
		//pomiar
		measure_time(1);
		gsl_blas_dgemm (CblasNoTrans, CblasNoTrans,
                  1.0, &A.matrix, &B.matrix,
                  0.0, &C.matrix);
		measure_time(0);
		//koniec_pomiaru
	}
	else if(strcmp(type, "naive") == 0){
		
		
		//pomiar
		measure_time(1);
		for(int j=0; j<vect; j++){
			for(int k=0; k<vect; k++){
				for(int i=0; i < vect; i++){
					c[vect*i + j] += a[vect*i + k]*b[vect*k + j];
				}
			}
		}
		measure_time(0);
		//koniec_pomiaru
	}
	else if(strcmp(type, "better") == 0){

		
		//pomiar
		measure_time(1);
		for(int i=0; i<vect; i++){
			for(int k=0; k<vect; k++){
				for(int j=0; j < vect; j++){
					c[vect*i + j] += a[vect*i + k]*b[vect*k + j];
				}
			}
		}
		measure_time(0);
		//koniec_pomiaru

	}
}

int main(){
	init_arrays();

	printf("\"type\";\"size\";\"time\"\n");


	for(int vect = 100; vect <= 500; vect+=50){
		for(int i=0; i<10; i++){
			printf("naive;%d;", vect);
			calculate("naive", vect);
			
			printf("better;%d;", vect);
			calculate("better", vect);
			
			printf("blas;%d;", vect);
			calculate("blas", vect);
		}
	}

	return 0;
}
