from math import pow, exp

def fact(x):
  if(x <= 1): 
    return 1;
  return x*fact(x-1)

def expo1(x, n):
  res = 0
  for i in range(n):
    res += pow(x, i)/fact(i)
  return res

def expo2(x, n):
  if x < 0:
    return 1/expo1(-x, n)
  return expo1(x, n)

print("Niestabilny:\t",expo1(-20, 100))
print("Stabilny:\t\t",expo2(-20, 100))
print("Biblioteczny:\t", exp(-20))