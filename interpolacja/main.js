function toCSScolor(hexColor){
	var res = "#";
	var hexString = hexColor.toString(16);
	while(hexString.length < 6) hexString = "0"+hexString;
	return res+hexString;
}

function getRed(col){
	col >>= 16;
	return col;
}

function getGreen(col){
	return (col&0x00ff00)>>8;
}

function getBlue(col){
	return col&0x0000ff;
}

var baseGrid = [[ 0xff0000, 0x00ff00, 0x0000ff ],
				[ 0xffffff, 0xffffff, 0xffffff ],
				[ 0x000000, 0x000000, 0x000000 ]];

var workingGrid = [];

function naiveScale(){
	var oldLength = workingGrid.length;
	for(var i=0; i<oldLength; i++){
		workingGrid[i].length *= 2;
	}
	workingGrid.length *= 2;

	/* interpolacja w wierszach */
	for(var i=0; i<oldLength; i++){
		var left = oldLength - 1;
		var right = oldLength*2 - 1;
		while(left >= 0){
			workingGrid[i][right] = workingGrid[i][left];
			right--;
			workingGrid[i][right] = workingGrid[i][left];
			right--;
			left--;
		}
	}

	/* interpolacja całych wierszy */
	var left = oldLength-1;
	var right = oldLength*2 - 1;
	while(left >= 0){
		workingGrid[right] = workingGrid[left].slice();
		right--;
		workingGrid[right] = workingGrid[left].slice();
		right--;
		left--;
	}

	/* rysowanie wyniku */
	drawWorkingGrid();
}

function bilinearScale(){

	oldTab = [];
	for(var i=0; i<workingGrid.length; i++) oldTab[i] = workingGrid[i].slice();

	naiveScale();
	
	workingGrid.length -= 1;
	for(var i=0; i<workingGrid.length; i++) workingGrid[i].length -= 1;

	/* obliczanie średniego koloru z sąsiadów */
	for(var i=0; i<workingGrid.length; i += 2){ //+=2, bo co drugi wymaga przeliczenia
		for(var j=1; j<workingGrid.length; j+= 2){
			workingGrid[i][j] = 256*256*parseInt( ( getRed(workingGrid[i][j-1])+getRed(workingGrid[i][j+1]) )/2 );
			workingGrid[i][j] += 256*parseInt( ( getGreen(workingGrid[i][j-1])+getGreen(workingGrid[i][j+1]) )/2 );
			workingGrid[i][j] += parseInt( ( getBlue(workingGrid[i][j-1])+getBlue(workingGrid[i][j+1]) )/2 );
		}
	}

	/* obliczanie średniego koloru z sąsiadów w kolumnach */
	for(var i=1; i<workingGrid.length; i += 2){ //+=2, bo co drugi wymaga przeliczenia
		for(var j=0; j<workingGrid.length; j+= 2){
			workingGrid[i][j] = 256*256*parseInt( (getRed(workingGrid[i-1][j])+getRed(workingGrid[i+1][j]))/2 );
			workingGrid[i][j] += 256*parseInt( (getGreen(workingGrid[i-1][j])+getGreen(workingGrid[i+1][j]))/2 );
			workingGrid[i][j] += parseInt( (getBlue(workingGrid[i-1][j])+getBlue(workingGrid[i+1][j]))/2 );
		}
	}

	/* reszta punktów */
	for(var i=1; i<workingGrid.length; i+= 2){
		for(var j=1; j<workingGrid.length; j+= 2){
			workingGrid[i][j] = 256*256*parseInt( (getRed(workingGrid[i-1][j])+getRed(workingGrid[i+1][j]) + getRed(workingGrid[i][j-1])+getRed(workingGrid[i][j+1]))/4 );
			workingGrid[i][j] += 256*parseInt( (getGreen(workingGrid[i-1][j])+getGreen(workingGrid[i+1][j]) + getGreen(workingGrid[i][j-1])+getGreen(workingGrid[i][j+1]))/4 );
			workingGrid[i][j] += parseInt( (getBlue(workingGrid[i-1][j])+getBlue(workingGrid[i+1][j]) + getBlue(workingGrid[i][j-1])+getBlue(workingGrid[i][j+1]))/4 );
		}
	}

	// for(var i=1; i<workingGrid.length-1; i+=2){
	// 	for(var j=0; j<workingGrid.length; j++){
	// 		workingGrid[i][j] = parseInt( (workingGrid[i-1][j] + workingGrid[i+1][j])/2 );
	// 	}
	// }

	/* rysowanie wyniku */
	drawWorkingGrid();
}

function bicubicScale(){

	oldTab = [];
	for(var i=0; i<workingGrid.length; i++) oldTab[i] = workingGrid[i].slice();

	naiveScale();
	
	workingGrid.length -= 1;
	for(var i=0; i<workingGrid.length; i++) workingGrid[i].length -= 1;

	/* obliczanie średniego koloru z sąsiadów */
	for(var i=0; i<workingGrid.length; i += 2){ //+=2, bo co drugi wymaga przeliczenia
		for(var j=1; j<workingGrid.length; j+= 2){
			workingGrid[i][j] = 256*256*parseInt( ( getRed(workingGrid[i][j-1])+getRed(workingGrid[i][j+1]) )/2 );
			workingGrid[i][j] += 256*parseInt( ( getGreen(workingGrid[i][j-1])+getGreen(workingGrid[i][j+1]) )/2 );
			workingGrid[i][j] += parseInt( ( getBlue(workingGrid[i][j-1])+getBlue(workingGrid[i][j+1]) )/2 );
		}
	}

	/* obliczanie średniego koloru z sąsiadów w kolumnach */
	for(var i=1; i<workingGrid.length; i += 2){ //+=2, bo co drugi wymaga przeliczenia
		for(var j=0; j<workingGrid.length; j+= 2){
			workingGrid[i][j] = 256*256*parseInt( (getRed(workingGrid[i-1][j])+getRed(workingGrid[i+1][j]))/2 );
			workingGrid[i][j] += 256*parseInt( (getGreen(workingGrid[i-1][j])+getGreen(workingGrid[i+1][j]))/2 );
			workingGrid[i][j] += parseInt( (getBlue(workingGrid[i-1][j])+getBlue(workingGrid[i+1][j]))/2 );
		}
	}

	/* reszta punktów */
	for(var i=1; i<workingGrid.length; i+= 2){
		for(var j=1; j<workingGrid.length; j+= 2){
			workingGrid[i][j] = 256*256*parseInt( (getRed(workingGrid[i-1][j])+getRed(workingGrid[i+1][j]) + getRed(workingGrid[i][j-1])+getRed(workingGrid[i][j+1]) + getRed(workingGrid[i-1][j-1])+getRed(workingGrid[i-1][j+1]) + getRed(workingGrid[i+1][j+1])+getRed(workingGrid[i+1][j-1]))/8 );
			workingGrid[i][j] += 256*parseInt( (getGreen(workingGrid[i-1][j])+getGreen(workingGrid[i+1][j]) + getGreen(workingGrid[i][j-1])+getGreen(workingGrid[i][j+1]) + getGreen(workingGrid[i-1][j-1])+getGreen(workingGrid[i-1][j+1]) + getGreen(workingGrid[i+1][j+1])+getGreen(workingGrid[i+1][j-1]))/8 );
			workingGrid[i][j] += parseInt( (getBlue(workingGrid[i-1][j])+getBlue(workingGrid[i+1][j]) + getBlue(workingGrid[i][j-1])+getBlue(workingGrid[i][j+1]) + getBlue(workingGrid[i-1][j-1])+getBlue(workingGrid[i-1][j+1]) + getBlue(workingGrid[i+1][j+1])+getBlue(workingGrid[i+1][j-1]))/8 );
		}
	}

	// for(var i=1; i<workingGrid.length-1; i+=2){
	// 	for(var j=0; j<workingGrid.length; j++){
	// 		workingGrid[i][j] = parseInt( (workingGrid[i-1][j] + workingGrid[i+1][j])/2 );
	// 	}
	// }

	/* rysowanie wyniku */
	drawWorkingGrid();
}

function drawWorkingGrid(){
	for(var i=0; i<workingGrid.length; i++){
		for(var j=0; j<workingGrid[i].length; j++){
			ctx.fillStyle = toCSScolor(workingGrid[i][j]);
			ctx.fillRect(j, i, 1, 1);
		}
	}
}

window.onload = function () {
	ctx = document.getElementById("canv").getContext("2d");
	ctx.moveTo(100,100);
	for(var i=0; i<baseGrid.length; i++) workingGrid[i] = baseGrid[i].slice();

	drawWorkingGrid();
	

};