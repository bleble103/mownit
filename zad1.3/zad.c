#include<stdio.h>
#include<gsl/gsl_interp.h>

int main(){
    float c = 1.1;
    int i;
    for(i=0; i<200; i++){
        gsl_ieee_printf_float(&c);
        printf("\n");
        c /= 2;
    }
    return 0;
}
