#include<stdio.h>
#include<time.h>
#include<gsl/gsl_blas.h>


double *a;
double *b;
double *c;

void init_arrays(){
	a = malloc(400*sizeof(double));
	b = malloc(400*sizeof(double));
	c = malloc(400*sizeof(double));
}

void measure_time(int start){
	static struct timespec tms;
	static struct timespec tme;
	if(start == 1){
		clock_gettime(CLOCK_REALTIME, &tms);
	}
	else if (start == 0){
		clock_gettime(CLOCK_REALTIME, &tme);
		long long diff = 1e9*tme.tv_sec + tme.tv_nsec - (1e9*tms.tv_sec + tms.tv_nsec);
		printf("%Ld\n", diff);
	}
}

void calculate(int level, int vect){
	if(level == 1){
		//pomiar
		measure_time(1);
		gsl_vector_view A = gsl_vector_view_array(a, vect);
		gsl_vector_view B = gsl_vector_view_array(b, vect);
		double result = 0.00;
		
		gsl_blas_ddot(&A.vector, &B.vector, &result);
		measure_time(0);
		//koniec_pomiaru
	}
	else if(level == 2){
		//pomiar
		measure_time(1);
		gsl_matrix_view A = gsl_matrix_view_array(a, vect, vect);
		gsl_vector_view B = gsl_vector_view_array(b, vect);
		gsl_vector_view C = gsl_vector_view_array(c, vect);
		
		
		gsl_blas_dgemv (CblasNoTrans,
                  1.0, &A.matrix, &B.vector,
                  0.0, &C.vector);
		measure_time(0);
		//koniec_pomiaru
	}
}

int main(){
	init_arrays();

	printf("\"level\";\"size\";\"time\"\n");


	for(int vect = 2; vect <= 20; vect++){
		for(int i=0; i<10; i++){
			printf("%d;%d;", 1, vect);
			calculate(1, vect);
			
			printf("%d;%d;", 2, vect);
			calculate(2, vect);
		}
	}

	return 0;
}
