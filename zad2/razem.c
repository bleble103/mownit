#include<stdio.h>
#include<time.h>
void measure_time(int start){
	static struct timespec tms;
	static struct timespec tme;
	if(start != 0){
		clock_gettime(CLOCK_REALTIME, &tms);
	}
	else{
		clock_gettime(CLOCK_REALTIME, &tme);
		long long diff = 1e9*tme.tv_sec + tme.tv_nsec - (1e9*tms.tv_sec + tms.tv_nsec);
		printf("%Ld", diff);
	}
}

void calculate(int level, int vect){
	int i=0;
	while(i < 10e6*(vect+level)){
		int b = i*i;
		i++;
	}
}

int main(){
	printf("\"size\";\"time1\";\"time2\"\n");
	
	for(int vect = 2; vect < 8; vect++){
		for(int i=0; i<10; i++){
			printf("%d;", vect);
			measure_time(1);
			calculate(1, vect);
			measure_time(0);
			printf(";");
			
			measure_time(1);
			calculate(2, vect);
			measure_time(0);
			printf("\n");
		}
	}

	return 0;
}
