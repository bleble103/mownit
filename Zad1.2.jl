
x = Float32[]
y = Float32[]
r = 1:1000
for i=r
    tmp = Float32(i)
    push!(x, tmp)
    push!(y, nextfloat(tmp)-tmp)
    println(nextfloat(tmp)-tmp)
end

using Plots
scatter(x, y)